---
title: "API Gratuita Facturación Electrónica - SUNAT"
description: ""
date: 2019-04-30T16:57:32-05:00
draft: false
---
{{% section class="" container="container content w-720" title="Facturación Electrónica desde tu servidor" %}}
Servicio que busca simplificar nuestra forma de interactuar con la facturación electrónica - SUNAT, orientada a suprimir complejidad.
{{% has-text class="centered has-text-grey" %}}
REST API simplificada para generar XML.
Basado en **GREENTER**
{{% /has-text %}}
{{% columns %}}
{{% column %}}
### Calculo
Calcula y desglosa los impuestos.
{{% /column %}}
{{% column %}}
### Envio
Envia los comprobantes para su validez fiscal
{{% /column %}}
{{% /columns %}}
{{% columns %}}
{{% column %}}
### XML
Construye el XML y el PDF acorde al estándar UBL 2.1
{{% /column %}}
{{% column %}}
### Integración
Integra al sistema que ya usas **PHP**, **PYTHON**, **.NET**, etc
{{% /column %}}
{{% /columns %}}

{{% columns %}}
{{% column %}}
### Almacenamiento
Almacena tus controbantes XML y facilita su busqueda
{{% /column %}}
{{% column %}}
### Mail
Envia a tus clientes los comprobantes
{{% /column %}}
{{% /columns %}}
{{% has-text class="centered" %}}
ayuda[ARROBA]pymes.com.pe

{{% button class="is-info is-large" href="/empezar" %}}EMPEZAR{{% /button %}} {{% button class="is-warning is-large" href="/empezar" %}}API{{% /button %}}
{{% /has-text %}}
{{% /section %}}
