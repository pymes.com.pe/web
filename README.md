# Web
API para facturación electrónica - SUNAT
``sh
hugo server
hugo server -w --noHTTPCache --disableFastRender -v
hugo new integration/instalacion
``
## Build & Deploy
### Build all the page
```sh
hugo
```
### Deploy
```sh
cd public
tar -czvf web.tar.gz *
scp web.tar.gz root@IP:/home/pymes/apps/pymes-com-pe
tar -xzvf web.tar.gz
chmod +r * -R
```

## Server configuration

Copy pymes-com-pe.conf to serve the page
```sh
scp pymes-com-pe.conf root@IP:/etc/apache2/sites-available
a2ensite pymes-com-pe.conf
systemctl reload apache2
```
### Create Certificate
Is required acme.sh
```sh
acme.sh --renew -d pymes-com-pe
acme.sh --issue -d pymes-com-pe -w /home/pymes/apps/pymes-com-pe

acme.sh --install-cert -d pymes-com-pe \
--cert-file /etc/apache2/2.4/ssl/pymes-com-pe-cert.pem \
--key-file /etc/apache2/2.4/ssl/pymes-com-pe-key.pem \
--fullchain-file /etc/apache2/2.4/ssl/pymes-com-pe.pem \
--reloadcmd "sudo systemctl reload apache2"
```
